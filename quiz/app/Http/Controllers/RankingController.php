<?php

namespace App\Http\Controllers;

use App\Quiz;
use Illuminate\Http\Request;

class RankingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->authorizeResource(Ranking::class, 'Ranking');
    }

    /**
     * QuizController constructor.
     * 
     * @return \Illuminate\Http\Response
     */
    public function  index()
    {
        $points = Points::all()->orderBy('punkte');
        return response($points)
    }
    
    public function getGivenPoints(Quiz $quiz) 
    {
        $points = Points::ofTheGivenQuiz($quiz) 
        return response($points)
    }

 /**
 * Store a newly created resource in storage.
 *
 * @param \Illuminate\Http\Request $request
 * @return \Illuminate\Http\Response
*/
public function store(Request $request)
{

    $point = new Points([
        'user_id' => $request->get('user_id'),
        'name' => $request->get('name'),
        'points' => $request->get('points'),
        'quiz_id' => $request->get('quiz_id'),
        'date' => $request->get('date')

    ]);

    $point->save();

    return response($point);


    $request->validate([
        'quiz_id' => 'required',
        'user_id' => 'required',
        'points' => 'required'
    ]);

    /**
     * Show resource
     *
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function show(Points $point)
    {
        return response($point);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param \App\Points $point
     * @return \Illuminate\Http\Response
     */
    public function destory(Points $point)
    {
        $point->delete();
        return response(null);

    }


     /**
     * Update resource
     *
     * 
     * @param \App\Points $point
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * 
     */
    public function update(Points $point, Request $request)
    {

        $point->fill($request-all());

        $point->save();

        return response(null, 200);

    }




 
    











}
