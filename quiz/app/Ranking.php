<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = [
        'id',
        'quiz_id',
        'user_id',

    ];

  
    public function quizzes()
    {
        return $this->belongsTo(Quiz::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public static function Scope()
    {
        return $query->where('quiz_id', $query->id)->OrderByDesc('points')->groupBy('user_id')->get(['user_id', 'quiz_id', 'name', ])
    }

}
   
