import Repository from 'axios';

const resource = '/Ranking';

export default {
    get() {
        return Repository.get(`${resource}`);
    },

    getQuiz(quizId) {
        return Repository.get(`${resource}/${quizId}`);
    },

    getUser(userId) {
        return Repository.get(`${resource}/${userId}`);
    },

}